CREATE TABLE IF NOT EXISTS `packages` (
  `ID` bigint(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `package_version` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dependencies` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `usage` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `files` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `database_tables` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;