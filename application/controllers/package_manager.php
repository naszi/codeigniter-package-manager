<?php 
class Package_manager extends CI_Controller{
    
    private $upload_temp_folder = './temp/';
	
	private $data = array();

    function __construct (){
		parent::__construct();

		//seting up variables
		$this->load->library('Unzip');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper("file");
        $this->load->model('Package');
    }
	
    function index(){
		$this->load->view('manage_packages');
    }
	function upload_archive(){
		$config['upload_path'] = $this->upload_temp_folder;
		$config['allowed_types'] = 'zip';
		$config['max_size']     = '10000';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload()){
		    $this->data['errors'] = array('error' => $this->upload->display_errors());
		}else{
		    $this->data['upload_data'] = $this->upload->data();
			$this->unzip->allow(array('php', 'sql', 'txt'));
			$extract_folder = $this->data['upload_data']['file_path']."/".$this->data['upload_data']['raw_name'];
			$this->unzip->extract($this->data['upload_data']['full_path'], $extract_folder);
			
			$file_structure_package = $this->Package->get_file_structure($extract_folder);
			$instalation_path = $this->Package->get_instalation_path($file_structure_package, $extract_folder);
			
			//check if file with same name exists
			$existing_files = $this->Package->target_files_exists($instalation_path);
			if(count($existing_files)>0){
				$this->data['errors']['files_already_exists'] = $existing_files;
			}
			
		}
		$this->load->view('manage_packages', $this->data);
    }
	
    function clear_temp(){
		//cleaning uploaded and unzipped files
		delete_files($this->upload_temp_folder, TRUE);
		$this->load->view('manage_packages');
    }
}

?>