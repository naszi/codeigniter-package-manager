<?php defined('BASEPATH') or exit('No direct script access allowed');
class Package extends CI_Model{
        
	private $file_structure = array();
		    
    function __construct (){
        parent::__construct();
    }
    function get_file_structure($folder_path){
		
		foreach(get_dir_file_info($folder_path, TRUE) as $entity => $info){
			if(is_dir ($folder_path.'/'.$entity)){
				$this->get_file_structure($folder_path.'/'.$entity);
			}else{
				$this->file_structure[] = $folder_path.'/'.$entity;
			}
		}
		return $this->file_structure;
	}
	function get_instalation_path($file_structure, $extract_folder){
		$file_structure_instalation_path = array();
		$file_structure_instalation_path['source_path'] = array();
		$file_structure_instalation_path['instalaion_path'] = array();
		foreach($file_structure as $path){
			$file_structure_instalation_path['source_path'][]=$path;
			$instalation_path = ".".substr($path, strlen($extract_folder));
			$file_structure_instalation_path['instalaion_path'][]=$instalation_path;
		}
		return $file_structure_instalation_path;
	}
	function target_files_exists($file_structure_instalation_path){
		$existing_files = array();
		foreach($file_structure_instalation_path['instalaion_path'] as $target_file){
			if(is_file($target_file)){
				$existing_files[] = $target_file;
			}
		}
		return $existing_files;
	}
}

?>